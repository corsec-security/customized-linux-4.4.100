# Customized Linux 4.4.100  

Contains the 4.4.100 Linux kernel with the crypto and security modules updated to the 4.6.7 versions, along with the necessary modificatitions to integrate those modules with 4.4.100.  


## Patch File  

For ease of patching, includes a prepared [patch file](updates.patch) that updates the source code.  
Note: this patch only updates the source code and does not include the files mentioned below ending in `.4.6.7`.  


### License  

The 4.4.100 license is in [COPYING](COPYING) and the 4.6.7 license is in [COPYING.4.6.7]  
Note: the 4.4.100 and 4.6.7 COPYING files are identical.  


### CREDITS and MAINTAINERS  

The 4.4.100 credits and maintainers files are in [CREDITS](CREDITS) and [MAINTAINERS](MAINTAINERS). The 4.6.7 files are in [CREDITS.4.6.7](CREDITS.4.6.7) and [MAINTAINERS.4.6.7](MAINTAINERS.4.6.7).  