1. Compile kernel_integrity_test.c. The basic call to build it is `gcc -o kernel_integrity_test kernel_integrity_test.c -lcrypto`. This is included in the Makefile.  

2. Build the kernel through assembling the initrd. If you know how to modify the initrd build process, then go ahead and do that for the next few steps, otherwise wait until it's been built and then extract it. It can be extracted the following way `(cpio -i; unxz | cpio -id) < /path/to/initrd` (you may need to add another `cpio -i`, i.e. `(cpio -i; cpio -i; unxz | cpio -id) < /path/to/initrd`). However, since we're reassembling the files afterwards, it's better to make different folders for the parts: `mkdir a, b;(cd a; cpio -i; cd ../b; unxz | cpio -id) < /path/to/initrd` **All paths from here on are relative to the initrd root unless otherwise specified!**  

4. Copy the compiled vmlinuz image and the `kernel_integrity_test` binary to the initrd root directory. Make sure that libcrypto is available on the initrd (some may have it already, but make sure it's the right version), in the same place that the PATH_TO_INTEGRITY_LIBCRYPTO macro points to (defined in the `crypto/testmgr.h` file of the kernel source, it's `/usr/lib` by default).

5. Modify gen_kern_hmac as needed. By default, the cryptography .ko files are in `/lib/modules/[kernel_number]/kernel/crypto` and vmlinuz is in `/`.  

6. Run gen_kern_hmac. If gen_kern_hmac has been properly modified, this will produce a file called `kernel_hmacs` in the root of the initrd dir.  

7. (Re-)Assemble the initrd. If you extracted with folders as mentioned in (2), it can be reassembled like so:  
```
cd a
find . | cpio -H newc -o>../first_initrd
cd ../b
find . | cpio -H newc -o |xz >../main_initrd
cat first_initrd second_initrd > path/to/initrd/output
```  
Move the initrd where it has to go and install the kernel, and you're done.